#pragma once

namespace DataTypes {

	enum FileType {
		DISK,
		DIRECTORY,
		SECRET,
		PROGRAM
	};

	enum Rights
	{
		NO_ACCESS,
		READ, // R
		READ_WRITE // RW
	};

	enum Secrecy
	{
		NON_CONFIDENTIAL,
		CONFIDENTIAL,
		STRICTLY_CONFIDENTIAL
	};

}