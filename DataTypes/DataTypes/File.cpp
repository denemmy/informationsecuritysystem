#include "File.h"

namespace DataTypes {
	File::File() {
		//ctor
	}

	File::~File() {
		//dtor
	}

	File::File(std::string name, FileType fileType, Secrecy secrecy, int group, Rights rights, std::string text, std::string creator) {
		this ->name = name;
		this ->type = fileType;
		this ->secrecy = secrecy;
		this ->group = group;
		this ->rights = rights;
		this ->text = text;
		this ->controlSum = 0;
		this ->creatorName = creator;
	}
}