#include "User.h"


namespace DataTypes {
	User::User() {
		//TODO
	}
	
	User::~User() {
		//TODO
	}
	
	User::User(std::string name, std::string password, int group, Secrecy secrecy, Rights rightsDirectory, Rights rightsFile) {
		this->name = name;
		this->password = password;
		this->group = group;
		this->secrecy = secrecy;
		this->rightsDirectory = rightsDirectory;
		this->rightsFile = rightsFile;
	}
}
