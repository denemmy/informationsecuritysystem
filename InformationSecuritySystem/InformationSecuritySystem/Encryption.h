#pragma once

#include <string>

namespace Encryption {

	class Encryption {
    public:
        Encryption();
        ~Encryption();

        std::string encrypt(std::string& text);
        std::string decrypt(std::string& text);

        std::string getHash(std::string& str);

    protected:
    private:
};

}