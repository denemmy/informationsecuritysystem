#include "InformationSecuritySystem.h"

#include "time.h"

namespace InformationSecuritySystem {

	InformationSecuritySystem::InformationSecuritySystem(): logger(dataBase){
		dataBase = DataBase::DataBase();
		//logger = Logger::Logger(dataBase);
		encryption = Encryption::Encryption();
		integritySubsystem = IntegritySubsystem::IntegritySubsystem();
		/**
		TODO:
			1. Load database
			2. Load logger
			3. Load Encryption
			4. Load IntegritySubsystem
			5. Load admin
		**/
	}

	InformationSecuritySystem::~InformationSecuritySystem() {
		/**
		TODO:
			1. Save all information into DB
			2. Free memory
		**/
	}

	void InformationSecuritySystem::createUser(std::string name, std::string password, int group, DataTypes::Secrecy secrecy, DataTypes::Rights rigthsDirectory, DataTypes::Rights rigthsFile) {
		DataTypes::User user = DataTypes::User(name, password, group, secrecy, rigthsDirectory, rigthsFile);
		dataBase.addUser(user);
		/**
		TODO:
			1. Create user with params
			2. Append new user to users
			3. Save user in DB
		**/
	}

	void InformationSecuritySystem::createAdmin(std::string name, std::string password) {
		DataTypes::User user = DataTypes::User(name, password, 0, DataTypes::STRICTLY_CONFIDENTIAL, DataTypes::READ_WRITE, DataTypes::READ_WRITE);
		dataBase.addUser(user);
		/**
		TODO:
			1. Create admin with params (hint: use createUser)
			2. Append admin to users
			3. Update field admin
			4. Save admin in DB
		**/
	}

	void InformationSecuritySystem::removeUser(std::string name) {
		dataBase.removeUser(name);
		/**
		TODO:
			1. Remove user from users
			2. Remove user from DB
		**/
	}

	void InformationSecuritySystem::updateUser(std::string name, int group, DataTypes::Secrecy secrecy, DataTypes::Rights rightsDirectory, DataTypes::Rights rightsFile) {
		DataTypes::User oldUser = dataBase.getUser(name);
		DataTypes::User user = DataTypes::User(name, oldUser.getPassword(), group, secrecy, rightsDirectory, rightsFile);
		dataBase.updateUser(name, user);
		/**
		TODO:
			1. Update user with name
			2. Update user into DB
		**/
	}

	DataTypes::User InformationSecuritySystem::getUser(std::string name) {
		//TODO
		DataTypes::User user = dataBase.getUser(name);
		return user;
	}

        void InformationSecuritySystem::setCurrentUser(std::string name)
        {
            userName = name;
        }

	

        std::string InformationSecuritySystem::getCurrentUser()
        {
            return userName;
        }

        bool InformationSecuritySystem::isAdmin()
        {
			DataTypes::User user = dataBase.getUser(userName);
            if(user.getGroup() == 0)
                return true;
            return false;
        }

	void InformationSecuritySystem::createFile(std::string name, DataTypes::FileType fileType, DataTypes::Secrecy secrecy, int group, DataTypes::Rights rights, std::string text, std:: string creatorName) {
		DataTypes::File file = DataTypes::File(name, fileType, secrecy, group, rights, text, creatorName);
		dataBase.addFile(file);
		/**
		TODO:
			1. Create file with params
			2. Append new file to files
			3. Save file in DB
		**/
	}

	void InformationSecuritySystem::updateFile(std::string name, DataTypes::FileType fileType, DataTypes::Secrecy secrecy, int group, DataTypes::Rights rights, std::string text, std:: string creatorName) {
		DataTypes::File file = DataTypes::File(name, fileType, secrecy, group, rights, text, creatorName);
		dataBase.updateFile(name, file);
		/**
		TODO:
			1. Update file
			3. Update file in DB
		**/
	}
	void InformationSecuritySystem::removeFile(std::string name) {
		dataBase.removeFile(name);
		/**
		TODO:
			1. Remove file from users
			2.
			Remove file from DB
		**/
	}

	DataTypes::File InformationSecuritySystem::getFile(std::string name) {
		return dataBase.getFile(name);
		// TODO
	}

	void InformationSecuritySystem::createLog(std::string userName, std::string text) {
		time_t timer = time(&timer);
		DataTypes::Log log = DataTypes::Log(userName, text, timer);
		dataBase.addLog(log);
		/**
		TODO:
			1. Get current time
			2. Create Log(userName, text, time)
			3. Add new Log to logger
		**/
	}

	std::vector<DataTypes::Log> InformationSecuritySystem::getLogs() {
		return dataBase.getAllLogs();
		/**
		TODO:
			Get all Logs from logger (hint: use method getAllLogs from class Logger)
		**/
	}

	bool InformationSecuritySystem::checkUserPassword(std::string name, std::string password) {
		std::string newPasswordHash = encryption.getHash(password);
		DataTypes::User user = dataBase.getUser(name);
		if (user.getPassword() == newPasswordHash) {
			return true;
		} else {
			return false;
		}
		/**
		TODO:
			1.Generate hash of password
			2.Check password
			3.Return TRUE if password is correct, else FALSE
		**/
	}

	bool InformationSecuritySystem::checkPermission(std::string fileName, DataTypes::Rights user_intention) {
		/**
		@ ���� ���������
		TODO:
		1. Check permission to file for current user (check permission for all way to file (according to specification))
		2. return TRUE if user has permission to file, else FALSE
		**/

		std::string s = fileName; // "c:/user/pavel/secret_file/secret_project/project.sln";
		std::string delimiter = "/";

		int step = 0;
		size_t pos = 0;
		std::string token;

		std::string disk_name;
		std::string file_name = s;
		std::string dir_name;

		while ((pos = s.find(delimiter)) != std::string::npos) {
			token = s.substr(0, pos);
			if (step >= 1)
			{
				dir_name += delimiter + token;
			}
			if (step == 0)
			{
				dir_name += token;
			}
			s.erase(0, pos + delimiter.length());
			if (step == 0)
				disk_name = token;
			step++;
		}

		DataTypes::User cur_user = dataBase.getUser(userName);

		DataTypes::File disk = dataBase.getFile(disk_name);
		DataTypes::File dir = dataBase.getFile(dir_name);
		DataTypes::File file = dataBase.getFile(file_name);

		if (disk.getRights() == DataTypes::NO_ACCESS)
			return false;

		if (dir.getRights() == DataTypes::NO_ACCESS)
			return false;

		if (file.getSecresy() > cur_user.getSecrecy())
			return false;

		if (cur_user.getGroup() == file.getGroup())
			return true;

		if (file.getRights() > user_intention)
			return false;

		return true;
	}
	/*void InformationSecuritySystem::updatePermission(std::string fileName, DataTypes::Rights rights) {
		DataTypes::File = dataBase.getFile(fileName);
		file.setRights(rights);
		dataBase.updateFile(fileName, file);
	}*/

	unsigned long long InformationSecuritySystem::computeControlSum(std::string fileName) {
		DataTypes::File file = dataBase.getFile(fileName);
		unsigned long long controlSum = computeControlSum(file.getText());
		file.setControlSum(controlSum);
		dataBase.updateFile(fileName, file);
		return controlSum;
		/**
		TODO:
			1. Get file from files for fileName
			2. Call IntegritySubsystem.computeControlSum(file)
			3. return control sum
		**/
	}
	bool InformationSecuritySystem::checkIntegrity(std::string fileName) {
		DataTypes::File file = dataBase.getFile(fileName);
		unsigned long long currentControlSum = computeControlSum(file.getText());
		if (currentControlSum == file.getControlSum()) {
			return true;
		} else {
			return false;
		}
		/**
		TODO:
			1. Get file from files for fileName
			2. Call IntegritySubsystem.checkIntegrity(file)
		**/
	}

	InformationSecuritySystem* InformationSecuritySystem::GetInstance()  { 
		static InformationSecuritySystem* iss; 
		if( iss == 0 ) { 
			iss = new InformationSecuritySystem; 
		} 
		return iss; 
	}
}
