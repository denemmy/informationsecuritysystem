#pragma once

#include "Log.h"
#include "DataBase.h"

#include <vector>

namespace Logger {

	class Logger
	{
		public:
            Logger (DataBase :: DataBase & _dataBase);
            ~Logger();

			void addLogMessage (DataTypes :: Log log);
			std::vector<DataTypes::Log> getAllLogs();
		protected:
		private:
            DataBase :: DataBase dataBase;
	};

}