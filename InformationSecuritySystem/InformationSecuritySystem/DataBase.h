#pragma once 

#include "DataEnums.h"
#include "User.h"
#include "File.h"
#include "Log.h"
#include "sqlite3.h"

#include <string>
#include <vector>
using std::string;

namespace DataBase {

	class DataBase
	{
		public:
			DataBase();
			virtual ~DataBase();

			void addUser(DataTypes::User& user);
			DataTypes::User getUser(const std::string& userName);
			void removeUser(const std::string& userName);
			void updateUser(const std::string& userName, DataTypes::User& user);


			void addFile(DataTypes::File& file);
			DataTypes::File getFile(const std::string& fileName);
			void removeFile(const std::string& fileName);
			void updateFile(const std::string& fileName, DataTypes::File& file);

			void addLog(DataTypes::Log& log);
			std::vector<DataTypes::Log> getAllLogs();

			static int callback(void *count, int argc, char **argv, char **azColName) {
				int *c = (int*)count;
				*c = atoi(argv[0]);
				return 0;
			}
		protected:
		private:
			sqlite3 *DB;
			bool isUserExist(const std::string& userName);
			bool isFileExist(const std::string& fileName);
	};

}
