#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
//#include <login.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
//    LogIn *login;
private slots:
/*    void showAccExitWindow();
    void showExitProgramWindow();*/
    void on_fileButton_clicked();
    void on_integritySystembutton_clicked();
    void on_jurnalButton_clicked();
    void on_createTextFileButton_clicked();
    void on_createFolderButton_clicked();
    void on_backButton_clicked();
    void on_pathLineEdit_editingFinished();
    void on_goToButton_clicked();
    void showAdminWindow();
    void on_openFileButton_clicked();
    void on_deleteFileButton_clicked();
};

#endif // MAINWINDOW_H
