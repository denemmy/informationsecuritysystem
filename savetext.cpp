#include "savetext.h"
#include "ui_savetext.h"

SaveText::SaveText(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SaveText)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Сохранение файла"));
}

SaveText::~SaveText()
{
    delete ui;
}

void SaveText::on_buttonBox_rejected()
{
    this->close();
}

void SaveText::on_buttonBox_accepted()
{
    QString fileName;
    QString secretryLevel;

    //Извлекаем введенную информацию
    fileName      = ui->fileNameEdit->text();
    secretryLevel = ui->secretryLevelBox->currentText();

//To Do Написать сохранение файла. (Важно! Пока в эту функцию не передана информация о текущей директории, над этим надо подумать!)
}
