#include "DataBase.h"

#include <string>
#include <iostream>

using std::cout;
using std::endl;

namespace DataBase {

	DataBase::DataBase() {
		int res;
		res = sqlite3_open("DB_SYS.db", &DB);
		if(res){
			throw std::exception("Can't open the database!");
			sqlite3_close(DB);
		}
	}

	DataBase::~DataBase() {
		sqlite3_close(DB);
	}

	void DataBase::addUser(DataTypes::User& user) {
		if(isUserExist(user.getName())){
			string error = "User " + user.getName() + " already exists! You can't add user with the same name.";
			throw std::exception(error.c_str());
		}
		else {
			char *error;
			string base_error = "Can't insert into SYS_USERS table! ";
			string sqlInsertLog = "INSERT INTO SYS_USERS VALUES('" +user.getName()+"','"+user.getPassword()+"',"+std::to_string(user.getGroup())+","+
				 std::to_string((int)user.getSecrecy())+","+std::to_string((int)user.getRightsFile())+","+std::to_string((int)user.getRightsDirectory())+");";
			int res = sqlite3_exec(DB, sqlInsertLog.c_str(), NULL, NULL, &error);
			if(res){
				string er = base_error + error;
				sqlite3_free(error);
				throw std::exception(er.c_str());
			}
		}
	} 

	DataTypes::User DataBase::getUser(const std::string& userName) {
		if(isUserExist(userName)){
			DataTypes::User user;
			sqlite3_stmt *stmt;
			string sql = "SELECT * from SYS_USERS WHERE Name = '" + userName +"';";
			sqlite3_prepare(DB, sql.c_str(), -1, &stmt, NULL);
			while( sqlite3_step( stmt ) == SQLITE_ROW ){
				string name((char*)sqlite3_column_text(stmt, 0));
				string pass((char*)sqlite3_column_text(stmt, 1));
				int group(sqlite3_column_int(stmt, 2));
				DataTypes::Secrecy secr = (DataTypes::Secrecy)sqlite3_column_int(stmt, 3);
				DataTypes::Rights rf = (DataTypes::Rights)sqlite3_column_int(stmt, 4);
				DataTypes::Rights rd = (DataTypes::Rights)sqlite3_column_int(stmt, 5);
				user.setName(name);
				user.setPassword(pass);
				user.setGroup(group);
				user.setSecrecy(secr);
				user.setRightsDirectory(rd);
				user.setRightsFile(rf);
			}
			sqlite3_finalize(stmt);
			return user;
		}
		else {
			string error = "User " + userName + " doesn't exist! You can't get it.";
			throw std::exception(error.c_str());
		}
	}

	void DataBase::removeUser(const std::string& userName) {
		if(isUserExist(userName)){
			char *error;
			string base_error = "Can't delete from SYS_USERS table! ";
			string sqlDelete = "DELETE FROM SYS_USERS WHERE Name = '"+userName+"';";
			int res = sqlite3_exec(DB, sqlDelete.c_str(), NULL, NULL, &error);
			if(res){
				string er = base_error + error;
				sqlite3_free(error);
				throw std::exception(er.c_str());
			}
		}
		else{
			string error = "User " + userName + " doesn't exist! You can't remove him.";
			throw std::exception(error.c_str());
		}
	}

	void DataBase::updateUser(const std::string& userName, DataTypes::User& user) {
		if(isUserExist(userName)){
			removeUser(userName);
			addUser(user);
		}
		else{
			string error = "User " + userName + " doesn't exist! You can't update him.";
			throw std::exception(error.c_str());
		}
	}


	void DataBase::addFile(DataTypes::File& file) {
		if(isFileExist(file.getName())){
			string error = "File " + file.getName() + " already exists! You can't add file with the same name.";
			throw std::exception(error.c_str());
		}
		else{
			char *error;
			string base_error = "Can't insert into SYS_FILES table! ";
			string sqlInsert = "INSERT INTO SYS_FILES VALUES('" +file.getName()+"','"+file.getCreatorName()+"',"+std::to_string((int)file.getType())+","+
				std::to_string((int)file.getSecresy())+","+std::to_string((int)file.getGroup())+","+std::to_string((int)file.getRights())+",'"+file.getText()+"',"+
				std::to_string(file.getControlSum())+");";
			int res = sqlite3_exec(DB, sqlInsert.c_str(), NULL, NULL, &error);
			if(res){
				string er = base_error + error;
				sqlite3_free(error);
				throw std::exception(er.c_str());
			}
		}
	}

	DataTypes::File DataBase::getFile(const std::string& fileName) {
		if(isFileExist(fileName)){
			DataTypes::File file;
			sqlite3_stmt *stmt;
			string sql = "SELECT * from SYS_FILES WHERE Name = '" + fileName +"';";
			sqlite3_prepare(DB, sql.c_str(), -1, &stmt, NULL);
			while( sqlite3_step( stmt ) == SQLITE_ROW ){
				string name((char*)sqlite3_column_text(stmt, 0));
				string creator((char*)sqlite3_column_text(stmt, 1));
				DataTypes::FileType type = (DataTypes::FileType)sqlite3_column_int(stmt, 2);
				DataTypes::Secrecy secr = (DataTypes::Secrecy)sqlite3_column_int(stmt, 3);
				int group(sqlite3_column_int(stmt, 4));
				DataTypes::Rights rf = (DataTypes::Rights)sqlite3_column_int(stmt, 5);
				string text((char*)sqlite3_column_text(stmt, 6));
				int sum(sqlite3_column_int(stmt, 7));

				file.setName(name);
				file.setCreatorName(creator);
				file.setType(type);
				file.setSecrecy(secr);
				file.setGroup(group);
				file.setRights(rf);
				file.setText(text);
				file.setControlSum(sum);
			}
			sqlite3_finalize(stmt);
			return file;
		}
		else {
			string error = "File " + fileName + " doesn't exist! You can't get it.";
			throw std::exception(error.c_str());
		}
	}

	void DataBase::removeFile(const std::string& fileName) {
		if(isFileExist(fileName)){
			char *error;
			string base_error = "Can't delete from SYS_FILES table! ";
			string sqlDelete = "DELETE FROM SYS_FILES WHERE Name = '"+fileName+"';";
			int res = sqlite3_exec(DB, sqlDelete.c_str(), NULL, NULL, &error);
			if(res){
				string er = base_error + error;
				sqlite3_free(error);
				throw std::exception(er.c_str());
			}
		}
		else{
			string error = "File " + fileName + " doesn't exist! You can't remove him.";
			throw std::exception(error.c_str());
		}
	}

	void DataBase::updateFile(const std::string& fileName, DataTypes::File& file) {
		if(isFileExist(fileName)){
			removeFile(fileName);
			addFile(file);
		}
		else{
			string error = "File " + fileName + " doesn't exist! You can't update him.";
			throw std::exception(error.c_str());
		}
	}


	void DataBase::addLog(DataTypes::Log& log) {
		char *error;
		string base_error = "Can't insert into SYS_LOGS table! ";
		string sqlInsertLog = "INSERT INTO SYS_LOGS VALUES('" +log.getUserName()+"'" +","+"'"+log.getText()+"'" +","+std::to_string(log.getTime())+");";
		int res = sqlite3_exec(DB, sqlInsertLog.c_str(), NULL, NULL, &error);
		if(res){
			string er = base_error + error;
			sqlite3_free(error);
			throw std::exception(er.c_str());
		}
	}

	std::vector<DataTypes::Log> DataBase::getAllLogs() {
		sqlite3_stmt *stmt;
		std::vector< DataTypes::Log> result;
		sqlite3_prepare(DB, "SELECT * from SYS_LOGS;", -1, &stmt, NULL);
		while( sqlite3_step( stmt ) == SQLITE_ROW )
		{
			string author((char*)sqlite3_column_text(stmt, 0));
			string text((char*)sqlite3_column_text(stmt, 1));
			time_t time(sqlite3_column_int(stmt, 2));
			DataTypes::Log lg(author, text, time);
			result.push_back(lg);
		}
		sqlite3_finalize(stmt);
		return result;
	}

	bool DataBase::isUserExist(const std::string& userName){
		int count = 0;
		string sqlCount = "SELECT COUNT(*) FROM SYS_USERS WHERE Name = '"+userName+"';";
		char *error;
		int res = sqlite3_exec(DB, sqlCount.c_str(), callback, &count, &error);
		if(res){
			sqlite3_free(error);
			throw std::exception(error);
		}
		if(count > 0)
			return true;
		else
			return false;
	}

	bool DataBase::isFileExist(const std::string& fileName){
		int count = 0;
		string sqlCount = "SELECT COUNT(*) FROM SYS_FILES WHERE Name = '"+fileName+"';";
		char *error;
		int res = sqlite3_exec(DB, sqlCount.c_str(), callback, &count, &error);
		if(res){
			sqlite3_free(error);
			throw std::exception(error);
		}
		if(count > 0)
			return true;
		else
			return false;
	}
}