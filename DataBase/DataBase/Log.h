#pragma once

#include <string>

namespace DataTypes {
	class Log
	{
		public:
			Log();
			virtual ~Log();
			Log(std::string userName, std::string text, time_t time);

			std::string getUserName(){ return userName;}
			std::string getText() { return text; }
			time_t getTime() { return time; }
			
			void setUserName(std::string userName) {this ->userName = userName;}
			void setText(std::string text) {this ->text = text;}
			void setTime(time_t time) {this ->time = time;}

		protected:
		private:
			std::string text;
			std::string userName;
			time_t time;
	};
}