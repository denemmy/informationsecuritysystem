#include "Encryption.h"

namespace Encryption {
	Encryption::Encryption() {
		/**
		TODO:
			1. Initiate
			2. Load library with crypto
		**/
	}

	Encryption::~Encryption() {
		//dtor
	}

	std::string Encryption::encrypt(std::string& text) {
		/**
		TODO:
			1. Encrypt text with crypto library
			2. Return new text
		**/
		return text;
	}

	std::string Encryption::decrypt(std::string& text) {
		/**
		TODO:
			1. Decrypt text with crypto library
			2. Return new text
		**/
		return text;
	}

	std::string Encryption::getHash(std::string& str) {
		/**
		TODO:
			1. Generate hash of string with crypto library
			2. Return hash
		**/
		return str;
	}

}