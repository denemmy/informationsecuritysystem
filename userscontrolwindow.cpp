#include "userscontrolwindow.h"
#include "ui_userscontrolwindow.h"
#include "changeuserprop.h"
#include <QMessageBox>
#include "userwindow.h"
#include "InformationSecuritySystem/InformationSecuritySystem/InformationSecuritySystem.h"

usersControlWindow::usersControlWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::usersControlWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Управление пользователями"));
}

usersControlWindow::~usersControlWindow()
{
    delete ui;
}

void usersControlWindow::on_closeButton_clicked()
{
    this->close();
}

void usersControlWindow::on_deleteUserButton_clicked()
{
//To Do Действие кнопки должно происходить в зависимости от выбранного пользователя. Если файл не выбран, лучше кнопку сдеать неактивной
    InformationSecuritySystem::InformationSecuritySystem *iss = InformationSecuritySystem::InformationSecuritySystem::GetInstance();//Проверить!

/*
//To Do получить имя пользователя
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, tr("Удаление файла"), tr("Вы уверены, что хотите удалить пользователя?"), QMessageBox::Yes|QMessageBox::No);
    if(reply == QMessageBox::Yes)
        iss->removeUser();*/
}

void usersControlWindow::on_editUserButton_clicked()
{
//To Do Передача пользователя в форму. Думаю надо написать отдельный конструктор.
    changeUserProp changeuserprop;
    changeuserprop.setModal(true);
    changeuserprop.exec();
}

void usersControlWindow::on_createUserButton_clicked()
{
    userWindow userwindow;
    userwindow.setModal(true);
    userwindow.exec();
}
