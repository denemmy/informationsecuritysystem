#include "texteditor.h"
#include "ui_texteditor.h"
#include "savetext.h"
#include <QMessageBox>

TextEditor::TextEditor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TextEditor)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Текстовый редактор"));
}

TextEditor::~TextEditor()
{
    delete ui;
}

void TextEditor::on_saveButton_clicked()
{
//аналогичная проблема с информацией о директории.
    SaveText saveText;
    saveText.setModal(true);
    saveText.exec();
}

void TextEditor::on_cancelButton_clicked()
{
    //Спрашиваем, точно ли пользователь хочет выйти
//To Do Написать проверку изменения текста.
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, tr("Выход"), tr("Вы уверены, что хотите выйти без сохранения?"), QMessageBox::Yes|QMessageBox::No);
    if(reply == QMessageBox::Yes)
        this->close();
}
