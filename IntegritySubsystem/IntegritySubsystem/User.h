#pragma once

#include "DataEnums.h"

#include <string>

namespace DataTypes {

	class User
	{
		public:
			User();
			virtual ~User();
			User(std::string name, std::string password, int group, Secrecy secrecy, Rights rigthsDirectory, Rights rigthsFile);

			std::string getName() {return name;}
			std::string getPassword() {return password;}
			int getGroup() {return group;}
			DataTypes::Secrecy getSecrecy() {return secrecy;}
			DataTypes::Rights getRightsDirectory() {return rightsDirectory;}
			DataTypes::Rights getRightsFile() {return rightsFile;}

			void setName(std::string name) {this ->name = name;}
			void setPassword(std::string pass) {this ->password = pass;}
			void setGroup(int group) {this ->group = group;}
			void setSecrecy(DataTypes::Secrecy secr) {this ->secrecy = secr;}
			void setRightsDirectory(DataTypes::Rights rightsDir) {this ->rightsDirectory = rightsDir;}
			void setRightsFile(DataTypes::Rights rightsF) {this ->rightsFile = rightsF;}
		protected:
		private:
			std::string name;
			std::string password;
			//����� ������. "-1" - ������ �����������
			int group;
			Secrecy secrecy;

			Rights rightsDirectory;
			Rights rightsFile;
			//TODO: add getters and setters if necessary
	};
}