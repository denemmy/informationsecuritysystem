#include "IntegritySubsystem.h"

namespace IntegritySubsystem {

	IntegritySubsystem :: IntegritySubsystem() {}

	IntegritySubsystem :: ~IntegritySubsystem() {}

	unsigned long long IntegritySubsystem :: computeControlSum(DataTypes::File file) {
        std::ifstream fileReader;
        fileReader.open(file.getName(), std::ifstream::binary | std::ifstream::in);
        unsigned char cur_symbol;
        unsigned hash_faq6(0), hash_rot13(0);
        while (!fileReader.eof()) {
            fileReader.read((char *)&cur_symbol, sizeof(cur_symbol));
            HashFAQ6_cycle(cur_symbol, hash_faq6);
            HashRot13(cur_symbol, hash_rot13);
        }
        HashFAQ6_end(hash_faq6);
        //unsigned long long hash = hash_faq6 + hash_rot13 * pow(2, 32);
		unsigned long long hash = hash_faq6 + hash_rot13 * (1LL << 32);
        fileReader.close();
		return hash;
	}

	bool IntegritySubsystem :: checkIntegrity(DataTypes::File file) {
        return (file.getControlSum() == computeControlSum(file)) ? true : false;
	}
    
    void IntegritySubsystem :: HashFAQ6_cycle(unsigned char &symb, unsigned &hash) {
        hash += symb;
        hash += (hash << 10);
        hash ^= (hash >> 6);
    }
    
    void IntegritySubsystem :: HashFAQ6_end(unsigned &hash) {
        hash += (hash << 3);
        hash ^= (hash >> 11);
        hash += (hash << 15);
    }
    
    void IntegritySubsystem :: HashRot13(unsigned char &symb, unsigned &hash) {
        hash += symb;
        hash -= (hash << 13) | (hash >> 19);
    }
}


