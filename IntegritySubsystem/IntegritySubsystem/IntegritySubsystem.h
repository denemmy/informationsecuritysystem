#pragma once

#include "File.h"
#include <fstream>


namespace IntegritySubsystem {

	class IntegritySubsystem
	{
		public:
			IntegritySubsystem();
            ~IntegritySubsystem();

			unsigned long long computeControlSum(DataTypes::File file);
			bool checkIntegrity(DataTypes::File file);
		protected:
		private:
            void HashFAQ6_cycle(unsigned char &symb, unsigned &hash);
            void HashRot13(unsigned char &symb, unsigned &hash);
            void HashFAQ6_end(unsigned &hash);
	};
}


