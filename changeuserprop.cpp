#include "changeuserprop.h"
#include "ui_changeuserprop.h"
#include <QMessageBox>
#include "InformationSecuritySystem/InformationSecuritySystem/InformationSecuritySystem.h"
#include "additionalfunctions.h"
#include "InformationSecuritySystem/InformationSecuritySystem/DataEnums.h"

changeUserProp::changeUserProp(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::changeUserProp)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Редактирование пользователя"));
}

changeUserProp::~changeUserProp()
{
    delete ui;
}

void changeUserProp::on_pushButton_clicked()
{
    QString loginText;
    QString accessLevelText;
    QString groupText;
    QString fileRightsText;
    QString folderRightsText;

    int groupNum;

    std::string stdLogin;//Проверить!

    DataTypes::Rights fileRights, folderRights;
    DataTypes::Secrecy secRights;

    InformationSecuritySystem::InformationSecuritySystem *iss = InformationSecuritySystem::InformationSecuritySystem::GetInstance();//Проверить!

    //Извлекаем введенную информацию
    loginText       = ui->loginLabel->text();
    accessLevelText = ui->accessLevelBox->currentText();
    groupText       = ui->groupBox->currentText();
    fileRightsText   = ui->fileRightsBox->currentText();
    folderRightsText = ui->folderRightsBox->currentText();

    //Конвертируем в STD
    stdLogin = loginText.toLocal8Bit().constData();//Проверить!

    //Получаем номер группы
    groupNum = getGroupNum(groupText);

    //Получаем права доступа
    fileRights   = getRights(fileRightsText);
    folderRights = getRights(folderRightsText);
    secRights    = getSecrecy(accessLevelText);

    //Сохраняем учетную запись
    iss->updateUser(stdLogin, groupNum, secRights, folderRights, fileRights);

    //Закрываем окно создания админа
    this->close();
}

void changeUserProp::on_pushButton_2_clicked()
{
    this->close();
}

