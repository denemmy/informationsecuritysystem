#include "admincreation.h"
#include "ui_admincreation.h"
#include <QMessageBox>

AdminCreation::AdminCreation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AdminCreation)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Создание администратора"));
}

AdminCreation::~AdminCreation()
{
    delete ui;
}

void AdminCreation::on_createAdminButton_clicked()
{
    QString loginText;
    QString passwordText;

    //Извлекаем введенную информацию
    loginText    = ui->loginLineEdit->text();
    passwordText = ui->passwordLineEdit->text();

//To Do Проверка соответствия логина и пароля требованиям
    if(false)
    {
        //Одно из полей пустое. Выдем сообщение.
        QMessageBox::warning(this, tr("Ошибка"), tr("Все поля должны быть заполнены"));
    }
    else
    {
        //Все в порядке
//To Do Сохранить учетную запись админа
        //Сохраняем учетную запись

        //Открываем окно основной программы
        mainWindow = new MainWindow();
        mainWindow->show();

        //Закрываем окно создания админа
        this->close();
    }
}

void AdminCreation::on_exitCreationButton_clicked()
{
    this->close();
}
