#include "userwindow.h"
#include "ui_userwindow.h"
#include <QMessageBox>
#include "InformationSecuritySystem/InformationSecuritySystem/DataEnums.h"
#include "InformationSecuritySystem/InformationSecuritySystem/InformationSecuritySystem.h"
#include "additionalfunctions.h"

userWindow::userWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::userWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Создание пользователся"));
}

userWindow::~userWindow()
{
    delete ui;
}

void userWindow::on_pushButton_clicked()
{
    QString loginText;
    QString passwordText;
    QString accessLevelText;
    QString groupText;
    QString fileRightsText;
    QString folderRightsText;

    int groupNum;

    std::string stdLogin;//Проверить!
    std::string stdPassword;//Проверить!

    DataTypes::Rights fileRights, folderRights;
    DataTypes::Secrecy secRights;

    InformationSecuritySystem::InformationSecuritySystem *iss = InformationSecuritySystem::InformationSecuritySystem::GetInstance();//Проверить!

    //Извлекаем введенную информацию
    loginText        = ui->loginLineEdit->text();
    passwordText     = ui->passwordLineEdit->text();
    accessLevelText  = ui->accessLevelBox->currentText();
    groupText        = ui->groupBox->currentText();
    fileRightsText   = ui->fileRightsBox->currentText();
    folderRightsText = ui->folderRightsBox->currentText();

    //Конвертируем в STD
    stdLogin = loginText.toLocal8Bit().constData();//Проверить!
    stdPassword = passwordText.toLocal8Bit().constData();//Проверить!

//To Do получение номера группы
    //Получаем номер группы
    groupNum = getGroupNum(groupText);

    //Получаем права доступа
    fileRights   = getRights(fileRightsText);
    folderRights = getRights(folderRightsText);
    secRights    = getSecrecy(accessLevelText);

//To Do Проверка соответствия логина требованиям
    if(stdPassword.empty())
    {
        //Одно из полей пустое. Выдем сообщение.
        QMessageBox::warning(this, tr("Ошибка"), tr("Задайте пароль пользователя"));
    }
//To Do Проверить, нет ли уже такого пользователя
    else if(false)
    {
        QMessageBox::warning(this, tr("Ошибка"), tr("Пользователь с таким именем уже существует"));
    }
    else
    {
        //Все в порядке
        //Сохраняем учетную запись
        iss->createUser(stdLogin, stdPassword, groupNum, secRights, folderRights, fileRights);

        //Закрываем окно создания админа
        this->close();
    }
}

void userWindow::on_pushButton_2_clicked()
{
    this->close();
}
