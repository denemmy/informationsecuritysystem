#pragma once

#include "DataBase.h"
#include "DataEnums.h"
#include "Encryption.h"
#include "File.h"
#include "IntegritySubsystem.h"
#include "Log.h"
#include "Logger.h"
#include "User.h"

#include <string>
#include <vector>

namespace InformationSecuritySystem {
	class InformationSecuritySystem
	{
		public:
			InformationSecuritySystem();
                        virtual ~InformationSecuritySystem();

			static InformationSecuritySystem* GetInstance();

                        /*static InformationSecuritySystem* GetInstance() {
                            if( ISS == 0 ) {
                                return ISS = new InformationSecuritySystem;
                            }
							return ISS;
                        }*/

			void createUser(std::string name, std::string password, int group, DataTypes::Secrecy secrecy, DataTypes::Rights rigthsDirectory, DataTypes::Rights rigthsFile);
			void createAdmin(std::string name, std::string hashPassword);
			void removeUser(std::string name);
			void updateUser(std::string name, int group, DataTypes::Secrecy secrecy, DataTypes::Rights rigthsDirectory, DataTypes::Rights rigthsFile);
                        void setCurrentUser(std::string name);//���������!
			DataTypes::User getUser(std::string name);
                        std::string getCurrentUser();
                        bool isAdmin();

			void createFile(std::string name, DataTypes::FileType fileType, DataTypes::Secrecy secrecy, int group, DataTypes::Rights rights, std::string text, std::string creatorName);
			void updateFile(std::string name, DataTypes::FileType fileType, DataTypes::Secrecy secrecy, int group, DataTypes::Rights rights, std::string text, std::string creatorName);
			void removeFile(std::string name);
			DataTypes::File getFile(std::string name);

			void createLog(std::string userName, std::string text);
			std::vector<DataTypes::Log> getLogs();

			bool checkUserPassword(std::string userName, std::string password);
			bool checkPermission(std::string fileName, DataTypes::Rights user_intention);
			//void updatePermission(std::string fileName, DataTypes::Rights rights);

			// �������� ����� computeControlSum ������� integritySubsystem
			unsigned long long computeControlSum(std::string fileName);
			// �������� ����� checkIntegrity ������� integritySubsystem
			bool checkIntegrity(std::string fileName);

			//TODO: add more methods
			//TODO: add getters and setters if necessary
		protected:
		private:
			// ��� ������ - ���������
			std::string adminName;
			// ��� �������� ������������
			std::string userName;

			// ��������� ������ DataBase, ���������� �� ������ � ����� ������
			DataBase::DataBase dataBase;

			// ��������� ������ Logger, ���������� �� ������ � ��������� �������� (������ ����������� � �����)
			Logger::Logger logger;

			// ��������� ������ Encryption, ���������� �� ������ � ������� (������ ����������������� ������)
			Encryption::Encryption encryption;

			// ��������� ������ IntegritySubsystem (������ ����������� �����������)
			IntegritySubsystem::IntegritySubsystem integritySubsystem;

                        static InformationSecuritySystem* iss;

	};
}
