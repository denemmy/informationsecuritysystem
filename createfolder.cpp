#include "createfolder.h"
#include "ui_createfolder.h"

CreateFolder::CreateFolder(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateFolder)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Создать папку"));
}

CreateFolder::~CreateFolder()
{
    delete ui;
}

//Сохранение папки
void CreateFolder::on_buttonBox_accepted()
{
    QString folderName;

    //Извлекаем введенную информацию
    folderName = ui->folderNameEdit->text();
//To Do Написать создание новой папки. (Важно! Пока в это функцию не передана информация о текущей директории, над этим надо подумать!)
}

void CreateFolder::on_buttonBox_rejected()
{
    this->close();
}
