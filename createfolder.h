#ifndef CREATEFOLDER_H
#define CREATEFOLDER_H

#include <QDialog>

namespace Ui {
class CreateFolder;
}

class CreateFolder : public QDialog
{
    Q_OBJECT

public:
    explicit CreateFolder(QWidget *parent = 0);
    ~CreateFolder();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::CreateFolder *ui;
};

#endif // CREATEFOLDER_H
