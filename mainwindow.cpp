#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMenu>
#include <QAction>
#include <QMessageBox>
#include "texteditor.h"
#include "createfolder.h"
#include "userscontrolwindow.h"
#include "InformationSecuritySystem/InformationSecuritySystem/InformationSecuritySystem.h"//Проверить!

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Основное окно"));
    InformationSecuritySystem::InformationSecuritySystem *iss = InformationSecuritySystem::InformationSecuritySystem::GetInstance();//Проверить!

    //Создаем строку меню
    QMenu *menu = menuBar()->addMenu("Меню");
    QAction *accExit = new QAction(tr("Выход пользователя"), this);
    QAction *programExit = new QAction(tr("Выход из программы"), this);
    menu->addAction(accExit); menu->addAction(programExit);

    //Меню администратора
    if(iss->isAdmin())//Проверить!
    {
        //Создаем подпункт в меню для администратора
        QMenu *adminMenu = menuBar()->addMenu("Меню администратора");
        QAction *usersListAdministrtion = new QAction(tr("Управление пользователями"), this);
        adminMenu->addAction(usersListAdministrtion);
        connect(usersListAdministrtion, SIGNAL(triggered()), this, SLOT(showAdminWindow()));
    }

    //Шорткаты для пунктов меню
    programExit->setShortcut(tr("Ctrl+Q"));
    accExit->setShortcut(tr("Ctrl+E"));

//To Do Разобраться с действиями кнопок меню. при компиляции выдает ошибку, хотя код вроде правельный.
//    connect(accExit, SIGNAL(triggered()), this, SLOT(showAccExitWindow));
//    connect(programExit, SIGNAL(triggered()), this, SLOT(showExitProgrammWindow()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showAdminWindow()
{
    //Выводим меню администратора
    usersControlWindow userControlWindow;
    userControlWindow.setModal(true);
    userControlWindow.exec();
}

//Обработчики пунктов меню выхода из программы и выхода польователя. Причины неработы непонятны.
/*void MainWindow::showAccExitWindow()
{
//    login = new LogIn();
    if(QMessageBox::question(this, tr("Выход"), tr("Вы уверены, что хотите выйти из учетной записи?"), 0))
    {
//To Do Выход тользователя

        //Открываем окно выбора пользователя
 //       login->show();

        //Закрываем текщее окно
        this->close();
    }
}

void MainWindow::showExitProgramWindow()
{
    if(QMessageBox::question(this, tr("Выход"), tr("Вы уверены, что хотите закрыть приложение?"), QMessageBox::Yes|QMessageBox::No))
    {
        //Закрываем текщее окно
        this->close();
    }
}*/

void MainWindow::on_fileButton_clicked()
{
    ui->fileButton->setChecked(1);
    ui->integritySystembutton->setChecked(0);
    ui->jurnalButton->setChecked(0);

    ui->pathLineEdit->setEnabled(1);
    ui->backButton->setEnabled(1);
    ui->goToButton->setEnabled(1);
    ui->addToIntegrityButton->setEnabled(1);
    ui->createTextFileButton->setEnabled(1);
    ui->createFolderButton->setEnabled(1);

//To Do Переключение таблицы
    //Переключаемся на диспетчер файлов
}

void MainWindow::on_integritySystembutton_clicked()
{
    ui->fileButton->setChecked(0);
    ui->integritySystembutton->setChecked(1);
    ui->jurnalButton->setChecked(0);

    ui->pathLineEdit->setEnabled(0);
    ui->backButton->setEnabled(0);
    ui->goToButton->setEnabled(0);
    ui->addToIntegrityButton->setEnabled(0);
    ui->createTextFileButton->setEnabled(0);
    ui->createFolderButton->setEnabled(0);

//To Do Переключение таблицы
    //Переключаемся на сситему контроля целосности
}

void MainWindow::on_jurnalButton_clicked()
{
    ui->fileButton->setChecked(0);
    ui->integritySystembutton->setChecked(0);
    ui->jurnalButton->setChecked(1);

    ui->pathLineEdit->setEnabled(0);
    ui->backButton->setEnabled(0);
    ui->goToButton->setEnabled(0);
    ui->addToIntegrityButton->setEnabled(0);
    ui->createTextFileButton->setEnabled(0);
    ui->createFolderButton->setEnabled(0);

//To Do Переключение таблицы
    //Переключаемся на журнал
}

//Создание нового файла
void MainWindow::on_createTextFileButton_clicked()
{
    TextEditor textEditor;
    textEditor.setModal(true);
    textEditor.exec();
}

//Создание новой папки.
void MainWindow::on_createFolderButton_clicked()
{
    CreateFolder createFolder;
    createFolder.setModal(true);
    createFolder.exec();
}

//Кнопка назад
void MainWindow::on_backButton_clicked()
{
//To Do Написать смену дериктории
    //ui->listView->
}

//Строка пути. Видимо обработчик нажатия Enter, но я не уверен. Надо проверять.
void MainWindow::on_pathLineEdit_editingFinished()
{
//To Do Написать смену дериктории
}

//Кнопка перейти.
void MainWindow::on_goToButton_clicked()
{
//To Do Написать смену дериктории (аналогично on_pathLineEdit_editingFinished())
}

void MainWindow::on_openFileButton_clicked()
{
//To Do Если файл не выбран, лучше кнопку сдеать неактивной
    TextEditor textEditor;
    textEditor.setModal(true);
    textEditor.exec();
}

void MainWindow::on_deleteFileButton_clicked()
{
//To Do Действие кнопки должно происходить в зависимости от выбранного файла. Если файл не выбран, лучше кнопку сдеать неактивной
    InformationSecuritySystem::InformationSecuritySystem *iss = InformationSecuritySystem::InformationSecuritySystem::GetInstance();//Проверить!

/*
//To Do получить имя файла
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, tr("Удаление файла"), tr("Вы уверены, что хотите удалить файл?"), QMessageBox::Yes|QMessageBox::No);
    if(reply == QMessageBox::Yes)
        iss->removeFile();*/
}
