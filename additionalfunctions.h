#ifndef ADDITIONALFUNCTIONS
#define ADDITIONALFUNCTIONS

#include <QString>
#include "InformationSecuritySystem/InformationSecuritySystem/DataEnums.h"

int getGroupNum(QString group);
DataTypes::Rights getRights(QString string);
DataTypes::Secrecy getSecrecy(QString string);

#endif // ADDITIONALFUNCTIONS

