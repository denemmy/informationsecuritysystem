#ifndef SAVETEXT_H
#define SAVETEXT_H

#include <QDialog>

namespace Ui {
class SaveText;
}

class SaveText : public QDialog
{
    Q_OBJECT

public:
    explicit SaveText(QWidget *parent = 0);
    ~SaveText();

private slots:
    void on_buttonBox_rejected();

    void on_buttonBox_accepted();

private:
    Ui::SaveText *ui;
};

#endif // SAVETEXT_H
