#include "login.h"
#include "ui_login.h"
#include <QMessageBox>
#include "InformationSecuritySystem/InformationSecuritySystem/InformationSecuritySystem.h"//Проверить!

LogIn::LogIn(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LogIn)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Вход в систему"));
}

LogIn::~LogIn()
{
    delete ui;
}

void LogIn::on_loginButton_clicked()
{
    QString loginText;
    QString passwordText;

    std::string stdLogin;//Проверить!
    std::string stdPassword;//Проверить!

    InformationSecuritySystem::InformationSecuritySystem* iss = InformationSecuritySystem::InformationSecuritySystem::GetInstance();//Проверить!

    //Извлекаем введенную информацию
    loginText    = ui->loginLineEdit->text();//Проверить!
    passwordText = ui->passwordLineEdit->text();//Проверить!

    //Конвертируем в STD
    stdLogin = loginText.toLocal8Bit().constData();//Проверить!
    stdPassword = passwordText.toLocal8Bit().constData();//Проверить!

    // Проверка соответствия логина и пароля
    if(stdLogin.empty() || stdPassword.empty())//Проверить!
    {
        //Одно из полей пустое. Выдем сообщение.
        QMessageBox::warning(this, tr("Ошибка"), tr("Все поля должны быть заполнены"));
    }
    else if(~iss->checkUserPassword(stdLogin, stdPassword))//Проверить!
    {
        //Неправильные логин и пароль
        QMessageBox::warning(this, tr("Ошибка"), tr("Неправильные логин и пароль"));
    }
    else
    {
        //Все в порядке
        //Входим в учетную запись
        iss->setCurrentUser(stdLogin);//Проверить!

        //Открываем окно основной программы
        mainWindow = new MainWindow();
        mainWindow->show();

        //Закрываем окно входа
        this->close();
    }
}

void LogIn::on_exitCreationButton_clicked()
{
    this->close();
}
