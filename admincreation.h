#ifndef ADMINCREATION_H
#define ADMINCREATION_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
class AdminCreation;
}

class AdminCreation : public QDialog
{
    Q_OBJECT

public:
    explicit AdminCreation(QWidget *parent = 0);
    ~AdminCreation();

private slots:
    void on_createAdminButton_clicked();

    void on_exitCreationButton_clicked();

private:
    Ui::AdminCreation *ui;
    MainWindow *mainWindow;
};

#endif // ADMINCREATION_H
