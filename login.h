#ifndef LOGIN_H
#define LOGIN_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
class LogIn;
}

class LogIn : public QDialog
{
    Q_OBJECT

public:
    explicit LogIn(QWidget *parent = 0);
    ~LogIn();

private slots:
    void on_loginButton_clicked();

    void on_exitCreationButton_clicked();

private:
    Ui::LogIn *ui;
    MainWindow *mainWindow;
};

#endif // LOGIN_H
