#include <string>

#ifndef USER_H
#define USER_H


class User
{
    public:
        std::string name;
        std::string password;
        //����� ������. "-1" - ������ �����������
        int group;
        Secrecy secrecy;

        Rigths rigthsDirectory;
        Rigths rigthsFile;

        User();
        virtual ~User();
        User(string name, string password, int group, Secrecy secrecy, Rigths rigthsDirectory, Rigths rigthsFile);
    protected:
    private:
};

enum Rights
{
    NO_ACCESS,
    READ, // R
    READ_WRITE // RW
};

enum Secrecy
{
    NON_CONFIDENTIAL,
    CONFIDENTIAL,
    STRICTLY_CONFIDENTIAL
};

#endif // USER_H
