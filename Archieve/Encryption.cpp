#include "Encryption.h"

Encryption::Encryption() {
    /**
    TODO:
        1. Initiate
        2. Load library with crypto
    **/
}

Encryption::~Encryption() {
    //dtor
}

string Encryption::encrypt(string text) {
    /**
    TODO:
        1. Encrypt text with crypto library
        2. Return new text
    **/
}

string Encryption::decrypt(string text) {
    /**
    TODO:
        1. Decrypt text with crypto library
        2. Return new text
    **/
}

string Encryption::getHash(string str) {
    /**
    TODO:
        1. Generate hash of string with crypto library
        2. Return hash
    **/
}
