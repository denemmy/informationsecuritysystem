#include <time.h>
#include <string>

#ifndef LOG_H
#define LOG_H


class Log
{
    public:
        string text;
        string userName;
        time_t time;

        Log();
        virtual ~Log();
        Log(string userName, string text, time_t time);
    protected:
    private:
};

#endif // LOG_H
