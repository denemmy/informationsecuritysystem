#include <vector>
#include <string>
#include <map>

#include "IntegritySubsystem.h"
#include "Encryption.h"
#include "DataBase.h"
#include "Logger.h"
#include "User.h"
#include "File.h"
#include "Log.h"

#ifndef INFORMATIONSECURITYSYSTEM_H
#define INFORMATIONSECURITYSYSTEM_H


class InformationSecuritySystem
{
    public:
        // ��� ������ - ���������
        string adminName;
        // ��� �������� ������������
        string userName;

        // ���� - ��� ������������, �������� - ������� ������ User
        // �������� ��� ������������ �������
        Map <string, User> users;

        // ���� - ������ ���� �� �����, �������� - ������� ������ File
        // �������� ��� ����� �������
        Map <string, File> files;

        // ��������� ������ DataBase, ���������� �� ������ � ����� ������
        DataBase database;

        // ��������� ������ Logger, ���������� �� ������ � ��������� �������� (������ ����������� � �����)
        Logger logger;

        // ��������� ������ Encryption, ���������� �� ������ � ������� (������ ����������������� ������)
        Encryption encryption;

        // ��������� ������ IntegritySubsystem (������ ����������� �����������)
        IntegritySubsystem integritySubsystem;

        // �����������
        InformationSecuritySystem();
        // ����������
        virtual ~InformationSecuritySystem();

        void createUser(string name, string password, int group, Secrecy secrecy, Rigths rigthsDirectory, Rigths rigthsFile);
        void createAdmin(string name, string hashPassword);
        void removeUser(string name);
        void updateUser(string name, string password, int group, Secrecy secrecy, Rigths rigthsDirectory, Rigths rigthsFile);

        void createFile(string name, FileType fileType, Secrecy secrecy, int group, Rights rights, string text);
        void updateFile(string name, FileType fileType, Secrecy secrecy, int group, Rights rights, string text);
        void removeFile(string name);

        void createLog(string userName, string text);
        vector<Log> getLogs();

        bool checkPassword(string userName, string password);
        bool checkPermission(string fileName);
        void updatePermission(string fileName, Rights rights);

        // �������� ����� computeControlSum ������� integritySubsystem
        long computeControlSum(string fileName);
        // �������� ����� checkIntegrity ������� integritySubsystem
        bool checkIntegrity(string fileName);

        //TODO: add more methods
    protected:
    private:
};

#endif // INFORMATIONSECURITYSYSTEM_H
