#include <string>

#ifndef FILE_H
#define FILE_H


class File
{
    public:
        String name;
        FileType type;

        // only for "secret" files
        Secrecy secrecy;

        string creatorName;
        int group;

        // ����� ��� �������������, �� ���������� ���������� � �� �� ������ ���������
        Rigths rights;

        // text for secretFiles
        string text;

        long controlSum;

        File();
        virtual ~File();
        File(string name, FileType fileType, Secrecy secrecy, int group, Rights rights, string text);

        bool checkControlSum(long controlSum);
        bool checkIntegrity();

        void decrypt();
        void encrypt();

        //TODO: add more methods
    protected:
    private:
};

enum FileType {
    DISK,
    DIRECTORY,
    SECRET,
    PROGRAM
};

#endif // FILE_H
