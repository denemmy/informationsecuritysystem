#include "IntegritySubsystem.h"

IntegritySubsystem::IntegritySubsystem() {
    //ctor
}

IntegritySubsystem::~IntegritySubsystem() {
    //dtor
}

long IntegritySubsystem::computeControlSum(File file) {
    /**
    TODO:
        1. Compute control sum for file
        2. return control sum
    **/
}

bool IntegritySubsystem::checkIntegrity(File file) {
    /**
    TODO:
        1. computeControlSum for file
        2. Compare new controlSum with old control sum
        3. return TRUE if control sums are equal, else FALSE
    **/
}
