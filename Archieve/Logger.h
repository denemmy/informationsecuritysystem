#include <vector>

#include "Log.h"

#ifndef LOGGER_H
#define LOGGER_H


class Logger
{
    public:
        vector <Log> logger;

        Logger();
        virtual ~Logger();

        void addLogMessage(Log log);
        void removeLogMessage(Log log);

        vector<Log> getAllLogs();
    protected:
    private:
};

#endif // LOGGER_H
