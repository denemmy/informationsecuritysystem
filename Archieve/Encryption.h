#include <string>

#ifndef ENCRYPTION_H
#define ENCRYPTION_H


class Encryption
{
    public:
        Encryption();
        virtual ~Encryption();

        string encrypt(string text);
        string decrypt(string text);

        string getHash(string str);

    protected:
    private:
};

#endif // ENCRYPTION_H
