#include "File.h"

File::File() {
    //ctor
}

File::~File() {
    //dtor
}

File::File(string name, FileType fileType, Secrecy secrecy, int group, Rights rights, string text) {
    /**
    TODO:
        create instance of class File with input params
    **/
}

bool File::checkControlSum(long controlSum) {
    /**
    TODO:
        1. Compare input controlSum with controlSum of file
        2. return TRUE if sums are equal, else FALSE
    **/
}

bool File::checkIntegrity() {
    /**
    TODO:
        1. compute controlSum
        2. call checkControlSum(controlSum)
        3. return TRUE if sums are equal, else FALSE
    **/
}

string File::decryptText() {
    /**
    TODO:
        1. decrypt file's text via Encryption.decrypt(fileText)
        2. return decrypt version of text file
    **/
}

string encryptText() {
    /**
    TODO:
        1. encrypt file's text via Encryption.encrypt(fileText)
        2. return encrypt version of text file
    **/
}
