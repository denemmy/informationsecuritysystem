#ifndef DATABASE_H
#define DATABASE_H

#include "user.h"
#include "file.h"
#include "log.h"

class DataBase
{
    public:
        DataBase();
        virtual ~DataBase();

        void addUser(User user);
        void removeUser(User user);
        void updateUser(User user);

        void addLog(Log log);
        void removeLog(Log log);
        void updateLog(Log log);

        void addFile(File file);
        void removeFile(File file);
        void updateFile(File file);
        //TODO: add methods
    protected:
    private:
};

#endif // DATABASE_H
