#include "InformationSecuritySystem.h"

InformationSecuritySystem::InformationSecuritySystem()
{
    /**
    TODO:
        1. Load database
        2. Load logger
        3. Load Encryption
        4. Load IntegritySubsystem
        5. Load users
        6. Load files
        7. Load admin
    **/
}

InformationSecuritySystem::~InformationSecuritySystem()
{
    /**
    TODO:
        1. Save all information into DB
        2. Free memory
    **/
}

void InformationSecuritySystem::createUser(string name, string password, int group, Secrecy secrecy, Rigths rigthsDirectory, Rigths rigthsFile) {
    /**
    TODO:
        1. Create user with params
        2. Append new user to users
        3. Save user in DB
    **/
}

void InformationSecuritySystem::createAdmin(string name, string password) {
    /**
    TODO:
        1. Create admin with params (hint: use createUser)
        2. Append admin to users
        3. Update field admin
        4. Save admin in DB
    **/
}

void InformationSecuritySystem::removeUser(string name) {
    /**
    TODO:
        1. Remove user from users
        2. Remove user from DB
    **/
}

void InformationSecuritySystem::updateUser(string name, string password, int group, Secrecy secrecy, Rigths rigthsDirectory, Rigths rigthsFile) {
    /**
    TODO:
        1. Update user with name
        2. Update user into DB
    **/
}

void InformationSecuritySystem::createFile(string name, FileType fileType, Secrecy secrecy, int group, Rights rights, string text) {
    /**
    TODO:
        1. Create file with params
        2. Append new file to files
        3. Save file in DB
    **/
}

void InformationSecuritySystem::updateFile(string name, FileType fileType, Secrecy secrecy, int group, Rights rights, string text) {
    /**
    TODO:
        1. Update file
        3. Update file in DB
    **/
}
void InformationSecuritySystem::removeFile(string name) {
    /**
    TODO:
        1. Remove file from users
        2. Remove file from DB
    **/
}

void InformationSecuritySystem::createLog(string userName, string text) {
    /**
    TODO:
        1. Get current time
        2. Create Log(userName, text, time)
        3. Add new Log to logger
    **/
}

void InformationSecuritySystem::getLogs() {
    /**
    TODO:
        Get all Logs from logger (hint: use method getAllLogs from class Logger)
    **/
}

bool InformationSecuritySystem::checkUserPassword(string name, string password) {
    /**
    TODO:
        1.Generate hash of password
        2.Check password
        3.Return TRUE if password is correct, else FALSE
    **/
}

bool InformationSecuritySystem::checkPermission(string fileName) {
    /**
    TODO:
        1. Check permission to file for current user (check permission for all way to file (according to specification))
        2. return TRUE if user has permission to file, else FALSE
    **/
}
void InformationSecuritySystem::updatePermission(string fileName, Rights rights) {
    /**
    TODO:
        1.Change permission for all users (non-manager and non-group) as rights
    **/
}

long InformationSecuritySystem::computeControlSum(string fileName) {
    /**
    TODO:
        1. Get file from files for fileName
        2. Call IntegritySubsystem.computeControlSum(file)
        3. return control sum
    **/
}
bool InformationSecuritySystem::checkIntegrity(string fileName) {
    /**
    TODO:
        1. Get file from files for fileName
        2. Call IntegritySubsystem.checkIntegrity(file)
    **/
}
