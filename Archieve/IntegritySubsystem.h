#ifndef INTEGRITYSUBSYSTEM_H
#define INTEGRITYSUBSYSTEM_H

#include "file.h"

class IntegritySubsystem
{
    public:
        IntegritySubsystem();
        virtual ~IntegritySubsystem();

        long computeControlSum(File file);
        bool checkIntegrity(File file);
    protected:
    private:
};

#endif // INTEGRITYSUBSYSTEM_H
