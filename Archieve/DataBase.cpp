#include "DataBase.h"

DataBase::DataBase() {
    /**
    TODO:
        1. Initiate Db.
        2. Load driver.
    **/
}

DataBase::~DataBase() {
    //dtor
}

void DataBase::addUser(User user) {
    /**
    TODO:
        1. Add user into DB (table Users)
    **/
}
void DataBase::removeUser(User user) {
    /**
    TODO:
        1. Remove user from DB (table Users)
    **/
}
void DataBase::updateUser(User user) {
    /**
    TODO:
        1. Update user into DB (table Users)
    **/
}


void DataBase::addLog(Log log) {
    /**
    TODO:
        1. Add log into DB (table Logs)
    **/
}

void DataBase::removeLog(Log log){
    /**
    TODO:
        1. Remove log from DB (table Logs)
    **/
}

void DataBase::updateLog(Log log){
    /**
    TODO:
        1. Update log into DB (table Logs)
    **/
}


void DataBase::addFile(File file){
    /**
    TODO:
        1. Add file into DB (table Files)
    **/
}

void DataBase::removeFile(File file){
    /**
    TODO:
        1. Remove file from DB (table Files)
    **/
}

void DataBase::updateFile(File file){
    /**
    TODO:
        1. Update file into DB (table Files)
    **/
}
