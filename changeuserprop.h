#ifndef CHANGEUSERPROP_H
#define CHANGEUSERPROP_H

#include <QDialog>

namespace Ui {
class changeUserProp;
}

class changeUserProp : public QDialog
{
    Q_OBJECT

public:
    explicit changeUserProp(QWidget *parent = 0);
    ~changeUserProp();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::changeUserProp *ui;
};

#endif // CHANGEUSERPROP_H
