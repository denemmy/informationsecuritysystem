#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H

#include <QDialog>

namespace Ui {
class TextEditor;
}

class TextEditor : public QDialog
{
    Q_OBJECT

public:
    explicit TextEditor(QWidget *parent = 0);
    ~TextEditor();

private slots:
    void on_saveButton_clicked();

    void on_cancelButton_clicked();

private:
    Ui::TextEditor *ui;
};

#endif // TEXTEDITOR_H
