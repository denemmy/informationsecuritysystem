#ifndef USERSCONTROLWINDOW_H
#define USERSCONTROLWINDOW_H

#include <QDialog>

namespace Ui {
class usersControlWindow;
}

class usersControlWindow : public QDialog
{
    Q_OBJECT

public:
    explicit usersControlWindow(QWidget *parent = 0);
    ~usersControlWindow();

private slots:
    void on_closeButton_clicked();

    void on_deleteUserButton_clicked();

    void on_editUserButton_clicked();

    void on_createUserButton_clicked();

private:
    Ui::usersControlWindow *ui;
};

#endif // USERSCONTROLWINDOW_H
