#include <QString>
#include "InformationSecuritySystem/InformationSecuritySystem/DataEnums.h"

int getGroupNum(QString group)
{
    return -1;
}

DataTypes::Rights getRights(QString string)
{
    if(string.compare("Нет доступа"))
        return DataTypes::NO_ACCESS;
    else if(string.compare("Только чтение"))
        return DataTypes::READ;
    else
        return DataTypes::READ_WRITE;
}


DataTypes::Secrecy getSecrecy(QString string)
{
    if(string.compare("Нет доступа"))
        return DataTypes::NON_CONFIDENTIAL;
    else if(string.compare("Только чтение"))
        return DataTypes::CONFIDENTIAL;
    else
        return DataTypes::STRICTLY_CONFIDENTIAL;
}
