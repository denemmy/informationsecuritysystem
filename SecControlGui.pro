#-------------------------------------------------
#
# Project created by QtCreator 2015-12-12T22:16:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SecControlGui
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    admincreation.cpp \
    login.cpp \
    userwindow.cpp \
    userscontrolwindow.cpp \
    changeuserprop.cpp \
    texteditor.cpp \
    savetext.cpp \
    createfolder.cpp \
    sqlite3.c \
    additionalfunctions.cpp

HEADERS  += mainwindow.h \
    admincreation.h \
    login.h \
    userwindow.h \
    userscontrolwindow.h \
    changeuserprop.h \
    texteditor.h \
    savetext.h \
    createfolder.h \
    InformationSecuritySystem.h \
    sqlite3.h \
    additionalfunctions.h

FORMS    += mainwindow.ui \
    admincreation.ui \
    login.ui \
    userwindow.ui \
    userscontrolwindow.ui \
    changeuserprop.ui \
    texteditor.ui \
    savetext.ui \
    createfolder.ui

DISTFILES += \

    InformationSecuritySystem.lib \
    DB_SYS.db

LIBS += -L"$$_PRO_FILE_PWD_" -l"InformationSecuritySystem"
