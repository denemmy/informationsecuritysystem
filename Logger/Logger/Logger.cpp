#include "Logger.h"

namespace Logger {

    Logger::Logger(DataBase :: DataBase & _dataBase) : dataBase(_dataBase) {}

	Logger::~Logger() {}

	void Logger::addLogMessage(DataTypes::Log log) {
        dataBase.addLog(log);
	}

	std::vector<DataTypes::Log> Logger::getAllLogs() {
		return dataBase.getAllLogs();
	}

}
