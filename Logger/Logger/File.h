#pragma once

#include "DataEnums.h"

#include <string>

namespace DataTypes {

	class File
	{
		public:
			File();
			virtual ~File();
			File(std::string name, FileType fileType, Secrecy secrecy, int group, Rights rights, std::string text, std::string creatorname);

			//TODO: add more methods
			//TODO: add getters and setters if necessary

			std::string getName() {return name;}
			DataTypes::FileType getType() {return type;}
			DataTypes::Secrecy getSecresy() {return secrecy;}
			std::string getCreatorName() {return creatorName;}
			int getGroup() {return group;}
			DataTypes::Rights getRights() {return rights;}
			std::string getText() {return text;}
			long getControlSum() {return controlSum;}

			void setName(std::string name) {this ->name = name;}
			void setType(DataTypes::FileType filetype) {this ->type = filetype;}
			void setSecrecy(DataTypes::Secrecy secrecy) {this ->secrecy = secrecy;}
			void setCreatorName(std::string creatorname) {this ->creatorName = creatorname;}
			void setGroup(int gr) {this ->group = gr;}
			void setRights(DataTypes::Rights rights) {this ->rights = rights;}
			void setText(std::string text) {this ->text = text;}
			void setControlSum(long sum) {this ->controlSum = sum;}

		protected:
		private:
			std::string name;
			FileType type;

			// only for "secret" files
			Secrecy secrecy;

			std::string creatorName;
			int group;

			// ����� ��� �������������, �� ���������� ���������� � �� �� ������ ���������
			Rights rights;

			// text for secretFiles
			std::string text;

			long controlSum;

	};
}