#pragma once 

#include "DataEnums.h"
#include "User.h"
#include "File.h"
#include "Log.h"

#include <string>
#include <vector>

namespace DataBase {

	class DataBase
	{
		public:
			DataBase();
			virtual ~DataBase();

			void addUser(DataTypes::User& user);
			DataTypes::User getUser(const std::string& userName);
			void removeUser(const std::string& userName);
			void updateUser(const std::string& userName, DataTypes::User& user);


			void addFile(DataTypes::File& file);
			DataTypes::File getFile(const std::string& fileName);
			void removeFile(const std::string& fileName);
			void updateFile(const std::string& fileName, DataTypes::File& file);

			void addLog(DataTypes::Log& log);
			std::vector<DataTypes::Log> getAllLogs();
		protected:
		private:
	};

}
